package com.training.promo.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.training.promo.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
