package com.training.promo.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.promo.dao.ProductDao;
import com.training.promo.entity.Product;

@RestController
public class ProductController {

    @Autowired private ProductDao productDao;

    @GetMapping("/product/")
    public Iterable<Product> semuaProduk() {
    	
        return productDao.findAll();
    }
    
    @GetMapping("/hostinfo")
    public Map<String, Object> backendInfo(HttpServletRequest request) {
        Map<String, Object> info = new HashMap<>();
        info.put("hostname", request.getLocalName());
        info.put("ip", request.getLocalAddr());
        info.put("port", request.getLocalPort());
        return info;
    }

}
